"use strict";

import { changing } from "best-globals";
import {html} from "js-to-html";
import * as bestGlobals from "best-globals";
import "dialog-promise";

myOwn.messages.confirmarIncorporacion='confirmar incorporación';

myOwn.clientSides=changing(myOwn.clientSides,{
    subir:{
        update: true,
        prepare: function(depot:myOwn.Depot, fieldName:string){
            var td=depot.rowControls[fieldName];
            td.style.backgroundImage= "url('" +my.path.img+'import.png' + "')";
            td.style.backgroundRepeat= "no-repeat";
            td.style.paddingRight = "16px";
            td.style.backgroundPosition= "right";
            td.addEventListener('click',function(){
                td.style.backgroundColor='#DDF';
                var buttonFile=html.input({type:'file',style:'min-width:400px'}).create();
                var buttonConfirmImport=html.input({type:'button', value:my.messages.import}).create() as (HTMLInputElement&{dialogPromiseDone:(p:any)=>void});
                // var buttonConfirmImport=html.input({type:'button', value:my.messages.import}).create();
                var progressIndicator=html.div({class:'indicator'},' ').create();
                var progressBar=html.div({class:'progress-bar', style:'width:400px; height:8px;'},[progressIndicator]).create()
                var uploadingProgress=function(progress:myOwn.ProgressPacket){
                    if(progress.lengthComputable){
                        progressIndicator.style.width=progress.loaded*100/progress.total+'%';
                        progressIndicator.title=Math.round(progress.loaded*100/progress.total)+'%';
                    }else{
                        progressIndicator.style.backgroundColor='#D4D';
                        progressIndicator.title='N/D %';
                    }
                }
                buttonConfirmImport.addEventListener('click', function(){
                    var files = buttonFile.files;
                    buttonConfirmImport.value='cargando...';
                    buttonConfirmImport.disabled=true;
                    bestGlobals.sleep(100).then(function(){
                        return my.ajax.documento_upload({
                            primaryKeyValues:depot.primaryKeyValues,
                            files:files
                        },{uploading:uploadingProgress});
                    }).then(function(result:{subido:any}){
                        buttonConfirmImport.dialogPromiseDone(true);
                        td.setTypedValue(result.subido);
                    }).catch(function(err:Error){
                        return buttonConfirmImport.dialogPromiseDone(err);
                    });
                });
                simpleFormPromise({elementsList:[
                    my.messages.uploadFile.replace('$1',depot.row.archivo),
                    buttonFile,
                    html.br().create(),
                    buttonConfirmImport,
                    html.br().create(),
                    progressBar,
                ]}).then(function(message){
                    return alertPromise(message);
                });
            });
        }
    },
    incorporar:{
        update: false,
        prepare: function(depot:myOwn.Depot, fieldName:string){
            var td=depot.rowControls[fieldName];
            my.createSmartButton({
                buttonLabel:'incorporar',
                confirmMessage:
                    my.messages.confirmarIncorporacion
                    .replace('$1',depot.row.archivo)
                    .replace('$2',depot.row.renglon)
                ,
                mainFun:async function(opts){
                    return await my.ajax.incorporacion_incorporar({
                        primaryKeyValues:depot.primaryKeyValues
                    },opts.ajaxOpts);
                },
                insideElement:td,
                initialState:'active'
            });
        },
        xprepare: function(depot:myOwn.Depot, fieldName:string){
            var td=depot.rowControls[fieldName];
            td.style.backgroundImage= "url('" +my.path.img+'check-table.png' + "')";
            td.style.backgroundRepeat= "no-repeat";
            td.style.paddingRight = "16px";
            td.style.backgroundPosition= "right";
            td.addEventListener('click',async function(){
                td.style.backgroundColor='#DDF';
                try{
                    await confirmPromise(
                        my.messages.confirmarIncorporacion
                        .replace('$1',depot.row.archivo)
                        .replace('$2',depot.row.renglon)
                        ,{askForNoRepeat:my.messages.confirmarIncorporacion}
                    );
                    var result = await my.ajax.incorporacion_incorporar({
                        primaryKeyValues:depot.primaryKeyValues
                    });
                    td.setTypedValue(result.incorporado);
                    (depot.rowControls.borrar_v||{}).textContent=result.incorporados;
                }catch(err){
                    await alertPromise(err.message);
                }
            });
        }
    },
    borrar_v:{
        update: true,
        prepare: function(depot:myOwn.Depot, fieldName:string){
            var td=depot.rowControls[fieldName];
            td.style.backgroundImage= "url('" +my.path.img+'borrar_v.png' + "')";
            td.style.backgroundRepeat= "no-repeat";
            td.style.paddingRight = "16px";
            td.style.backgroundPosition= "right";
            td.addEventListener('click',function(){
                td.style.backgroundColor='#DDF';
                confirmPromise(
                    my.messages.confirmarBorrarValoresParaIncorporacion
                    .replace('$1',depot.row.archivo)
                    .replace('$2',depot.row.renglon)
                    ,{askForNoRepeat:my.messages.confirmarBorrarValoresParaIncorporacion}
                ).then(function(){
                    return my.ajax.indicadores_borrar_v({
                        primaryKeyValues:depot.primaryKeyValues
                    }).then(function(result){
                        td.setTypedValue(result.quedan);
                        (depot.rowControls.incorporado||{}).textContent='borrados: '+result.borrados;
                        return result;
                    })
                }).catch(my.alertError);
            });
        }
    }
});
