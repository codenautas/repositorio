"use strict";

import {TableDefinition, TableContext} from "./types-repositorio";

export function incorporaciones(context:TableContext):TableDefinition{
    var admin=context.user.rol==='admin';
    return {
        name:'incorporaciones',
        elementName:'incorporación',
        editable:admin,
        fields:[
            {name:'incorporar'          , typeName:'text'   , allow:{select:admin}, clientSide:'incorporar', editable:false},
            {name:'incorporado'         , typeName:'text'   , editable:false, label:'en qué momento'},
            {name:'borrar_v'            , typeName:'text'   , serverSide:true, allow:{select:admin}, clientSide:'borrar_v'  , editable:false, label:'incorporados'},
            {name:'operativo'           , typeName:'text'   , },
            {name:'archivo'             , typeName:'text'   , },
            {name:'renglon'             , typeName:'integer', },
            {name:'metodo'              , typeName:'text'   , options:['tabla_txt']  },
            {name:'origen'              , typeName:'text'   , description:'nombre de la solapa o archivo comprimido' },
            {name:'destino'             , typeName:'text'   , description:'nombre la tabla de datos' },
            {name:'separador'           , typeName:'text'   , options:[',',';','|',{option:'\t',label:'TAB'}], dataLength:1, label:'de campos'},
            {name:'delimitador_texto'   , typeName:'text'   , dataLength:1, label:'que encierra los "textos"'},
            {name:'preserva_intermedios', typeName:'boolean', defaultValue:false, description:'si preserva los archivos intermedio, úsese solo en modo exploratorio'},
            {name:'tabla_lineas'        , typeName:'text'   , visible:false , dataLength:58},
            {name:'texto_de_null'       , typeName:'text'   },
            {name:'punto_decimal'       , typeName:'text'   , nullable:false, dataLength:1, defaultValue:'.', options:['.',',']},
            {name:'formato_fecha'       , typeName:'text'   , nullable:false, dataLength:1, defaultValue:'i', options:[{option:'i', label:'ISO (year month day)'},{option:'f', label:'ESP (día mes año)'},{option:'e', label:'ENG (month day year)'}]},
            {name:'barra_fecha'         , typeName:'text'   , nullable:false, dataLength:1, defaultValue:'-', options:['-','/']},
            /*
            {name:'indicador'  , typeName:'text'   , label:'indicador(es)'   },
            {name:'periodo'    , typeName:'text'   , label:'periodo(s)'      },
            {name:'ut'         , typeName:'text'   , label:'unidad(es) territorial(es)'},
            {name:'valores'    , typeName:'text'   , label:'rango de valores'},
            {name:'filas'      , typeName:'text'   , label:'(periodo, indicador, ut o una)'},
            {name:'columnas'   , typeName:'text'   , label:'(periodo, indicador, ut o una)'},
            */
        ],
        primaryKey:['operativo', 'archivo', 'renglon'],
        foreignKeys:[
            {references:'archivos', fields:['operativo','archivo']}
        ],
        detailTables:[
            // {table: 'valores'  , fields:['id_carpeta', 'archivo', 'renglon'], abr:'V'},
        ],
        constraints:[
            {constraintType: 'unique', fields: ['tabla_lineas'] },
            {constraintType: 'unique', fields: ['destino'] }
        ],
        sql:{
            isTable:true,
            from:`(select *, '0' as borrar_v from incorporaciones)`
            /*
            from:`(
                select * from incorporaciones x,
                  lateral (select count(valor)::text as borrar_v from valores v where v.id_carpeta = x.id_carpeta and v.archivo=x.archivo and v.renglon=x.renglon) e
            )`,
            postCreateSqls: `alter table incorporaciones add constraint "RANGOS! o solo minúsculas, números y _" check (indicador ~ '^(.*[!]\\w+\\d+:\\w+\\d+|[a-z][a-z0-9_;]*)$');`
            */
        }
   };
}