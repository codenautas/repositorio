"use strict";

import {defConfig} from "./def-config";
import {AppDatosExtType} from "datos-ext";
import {procedures} from "./procedures-repositorio";
import {ClientModuleDefinition, Request, MenuDefinition } from "./types-repositorio";
export * from "./types-repositorio";

import { archivos }         from "./table-archivos";
import { incorporaciones }  from "./table-incorporaciones";
import { temas }            from "./table-temas";

export type Constructor<T> = new(...args: any[]) => T;
export function emergeAppRepositorio<T extends Constructor<AppDatosExtType>>(Base:T){
    
    return class AppRepositorio extends Base{

        constructor(...args:any[]){ 
            super(args);
        }

        clientIncludes(req:Request, hideBEPlusInclusions?:boolean):ClientModuleDefinition[]{
            return super.clientIncludes(req, hideBEPlusInclusions).concat(
                req && req.user?[
                    {type:'js', src: 'client/repositorio.js' },
                ]:[]
            ).concat([
                {type: 'css', file:'repositorio.css' },
            ])
        }

        async getProcedures(){
            return (await super.getProcedures()).concat(procedures);
        }

        configStaticConfig(){
            super.configStaticConfig();
            this.setStaticConfig(defConfig);
        }

        getMenu():MenuDefinition{
            let myMenuPart=[
                {menuType:'table', name:'temas'},
                {menuType:'table', name:'datos', table:'tabla_datos'},
                {menuType:'menu', name:'config', menuContent:super.getMenu().menu}
            ];
            let menu = {menu: myMenuPart};
            return menu;
        }

        prepareGetTables(){
            var be=this;
            super.prepareGetTables();
            this.getTableDefinition={
                temas,
                ...this.getTableDefinition,
                archivos,
                incorporaciones
            }
            be.appendToTableDefinition('operativos',function(tableDef, _context){
                tableDef.fields.unshift({
                    name:'tema', typeName:'text'
                });
                tableDef.foreignKeys!.push({
                    references:'temas', fields:['tema']
                });
                tableDef.detailTables!.push({
                    table:'archivos', fields:['operativo'], abr:'A'
                });
            });
        }

        async start(opts?: StartOptions):Promise<void>{
            await super.start(opts)
            var minutes=20;
            this.server.timeout = minutes*60*1000;
            console.log('time out',minutes,'minutes');
        }
    }
}

/*

http://localhost:3042/repo/menu#i=temas,temas&detailing={"EPH":{"operativos":{"EPH2018T2":{"archivos":{"EPH_usu_2_Trim_2018_txt.zip":{"incorporaciones":{}}}}}},"EAH":{"operativos":{}}}

http://localhost:3040/repo/menu#i=temas,temas&detailing={"*":{"operativos":{"*":{"archivos":{}}}}}

http://localhost:3040/repo/menu#i=temas,temas&detailing={"*":{"operativos":{"*":{"archivos":{"*":{"incorporaciones":{}}}}}}}

*/