"use strict";

import {TableDefinition, TableContext} from "./types-repositorio";

export function temas(context:TableContext):TableDefinition{
    var admin = context.user.rol === 'admin';
    return {
        name: 'temas',
        elementName: 'tema',
        editable: admin,
        fields: [
            { name: "tema"               , typeName: 'text'    },
            { name: "nombre"             , typeName: 'text'    },
            { name: "descripcion"        , typeName: 'text'    , label:"descripción"},
            { name: "productor"          , typeName: 'text'    },
            { name: "cobertura"          , typeName: 'text'    },
        ],
        primaryKey: ['tema'],
        detailTables:[
            {table:'operativos', fields:['tema'], abr:'O'}
        ]
    }
}
