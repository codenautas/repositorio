export var defConfig=`
server:
  port: 3042
  base-url: /repositorio
  session-store: memory
  paths:
    attachments: local-attachments
    working: local-working
db:
  motor: postgresql
  host: localhost
  database: repositorio_db
  schema: repositorio
  user: repositorio_user
  search_path: 
  - repositorio
install:
  dump:
    db:
      owner: repositorio_owner
      owner4special-scripts: repositorio_owner
      user4special-scripts: repositorio_user
      apply-generic-user-replaces: true
    admin-can-create-tables: true
    enances: inline
    skip-content: false
    scripts:
      prepare:
      - ../node_modules/operativos/install/rel_tabla_relacionada.sql
      post-adapt: 
      - para-install.sql
      - detect_type_fun.sql
      - ../node_modules/pg-triggers/lib/recreate-his.sql
      - ../node_modules/pg-triggers/lib/table-changes.sql
      - ../node_modules/pg-triggers/lib/function-changes-trg.sql
      - ../node_modules/pg-triggers/lib/enance.sql
login:
  table: usuarios
  userFieldName: usuario
  passFieldName: md5clave
  rolFieldName: rol
  infoFieldList: [usuario, rol]
  activeClausule: activo
  plus:
    maxAge-5-sec: 5000    
    maxAge: 864000000
    maxAge-10-day: 864000000
    allowHttpLogin: true
    fileStore: false
    skipCheckAlreadyLoggedIn: true
    loginForm:
      formTitle: Repositorio
      usernameLabel: usuario
      passwordLabel: md5clave
      buttonLabel: entrar
      formImg: img/login-lock-icon.png
    chPassForm:
      usernameLabel: usuario
      oldPasswordLabel: clave anterior
      newPasswordLabel: nueva clave
      repPasswordLabel: repetir nueva clave
      buttonLabel: Cambiar
      formTitle: Cambio de clave
  messages:
    userOrPassFail: el nombre de usuario no existe o la clave no corresponde
    lockedFail: el usuario se encuentra bloqueado
    inactiveFail: es usuario está marcado como inactivo
client-setup:
  title: Repositorio
  cursors: true
  lang: es
  menu: true
`;