import {promises as fs} from "fs";
import * as miniTools from "mini-tools";
import * as process from "process";
import {execFile} from "child_process";

function controlarExista<T extends {}>(donde:T, ques:(keyof T)[]){
    ques.forEach(que=>{
        if(!donde[que]){
            throw new Error(`falta "${que}" en el local-config.yaml`);
        }
    })
}

interface Config {
    db:{
        host:string|null
        database:string
        user:string
        password:string
        port:string|null
    },
    devel:true
}

async function executePsql(modo:'create'|string){
    try{
        var config = await miniTools.readConfig<Config>([
            "local-config.yaml"
        ]);
        controlarExista(config.db, ['database', 'user', 'password']);
        controlarExista(config, ['devel']);
        if(config.db.host && config.db.host!='localhost' && config.db.host!='127.0.0.1'){
            console.log('ERROR! Solo funciona con host=localhost');
            return;
        }
        var params = '--no-password -v ON_ERROR_STOP=on --quiet --pset pager=off'.split(' ');
        if(config.db.port){
            params.push('--port', config.db.port);
        }
        var env = process.env;
        params.push('--username','postgres');
        if(modo=='create'){
            params.push('--file','local-db-dump-create-db.sql');
        }else{
            params.push('--single-transaction');
            if(modo=='reload-schema'){
                // params.push('--username',config.db.user);
                // env = {...process.env, PGPASSWORD:config.db.password};
                params.push('--file','local-db-dump.sql');
                params.push('--dbname',config.db.database);
            }else{
                console.log(process.argv);
                console.error('NO DETECTA EL MODO O NO ES VALIDO. Alternativas:')
                console.error('npsql create')
                console.error('npsql reload-schema')
                return;
            }
        }
        var dir = await fs.opendir('C:\\Program Files\\PostgreSQL');
        var maxVersion:number=0;
        for await (const dirent of dir) {
            // @ts-ignore mezclo a propósito string con number
            var numeroVistoQuizas:number=dirent.name;
            if(!isNaN(numeroVistoQuizas)){
                if(numeroVistoQuizas>maxVersion){
                    // @ts-ignore mezclo a propósito string con number
                    maxVersion=dirent.name;
                }
            }
        }
        console.log('psql '+maxVersion);
        const ls  = execFile(
            `C:\\Program Files\\PostgreSQL\\${maxVersion}\\bin\\psql.exe`,
            params,
            {env}
        );
        ls.stdout!.on('data', (data) => {
            console.log(data);
        });
        ls.stderr!.on('data', (data) => {
            console.error(`!: ${data}`);
            if(/fe_sendauth/.test(data)){
                console.log('Sin contraseña.')
                console.log('Alternativas en https://www.postgresql.org/docs/current/libpq-pgpass.html')
            }
        });
        ls.on('close', (code) => {
            if(code==0){
                console.log('ok');
            }else{
                console.log(`psql exited with code ${code}`);
            }
        });            
    }catch(err){
        console.log(err);
        console.log('err.code', err.code);
        console.log('err.number', err.number);
        throw err;
    }
}

executePsql(process.argv[0].endsWith('node.exe')?process.argv[2]:process.argv[1]);
