"use strict";

import {TableDefinition, TableContext} from "./types-repositorio";

export function archivos(context:TableContext):TableDefinition{
    var admin=context.user.rol==='admin';
    return {
        name:'archivos',
        elementName:'archivo',
        editable:admin,
        fields:[
            {name:'subido'           , typeName:'timestamp' , inTable:true, clientSide:'subir'   , allow:{select:admin}, editable:false, label:'cuándo fue subido el archivo'},
            {name:'borrar_v'         , typeName:'text'   , serverSide:true, clientSide:'borrar_v', allow:{select:admin}, editable:false, label:'incorporados'},
            {name:'operativo'        , typeName:'text'   ,                               description:'Operativo de pertenencia'},
            {name:'archivo'          , typeName:'text'   , label:'nombre del documento', description:'nombre sin ruta del archivo que contiene el documento'},
            {name:'tipo'             , typeName:'text'   , label:'Tipo'                , description:'Tipo de información fichada'},
            {name:'titulo'           , typeName:'text'   , label:'Título'              , description:'Título del contenido de la información fichada'},
            {name:'descripcion'      , typeName:'text'   , label:'Descripción'         , description:'Descripción de la información fichada'},
            {name:'periodos'         , typeName:'text'   , label:'Periodo'             , description:'Periodo al que corresponde la información fichada (año, trimestre, mes)'},
            {name:'link_descarga'    , typeName:'text'   , label:'Link descarga'       , description:'Link a la página que permite descargar la información fichada'},
            {name:'f_descarga'       , typeName:'date'   , label:'Fecha descarga'      , description:'Fecha en la cual se descargó la información'},
            {name:'pal_clave'        , typeName:'text'   , label:'Palabras clave'      , description:'Palabras claves o descriptores que faciliten la búsqueda y acceso a la información fichada.'},
            {name:'per_actualizacion', typeName:'text'   , label:'periodicidad'        , description:'Periodicidad de actualización de la información.'},
            {name:'estado'           , typeName:'text'   , label:'Estado'              , description:'Estado de la ficha. Este campo permite diferenciar en qué etapa se encuentra el proceso de fichado de la información. Por ejemplo: borrador, duda, candidato, definitivo.'},
        ],
        primaryKey:['operativo', 'archivo'],
        foreignKeys:[
            {references:'operativos', fields:['operativo']}
        ],
        detailTables:[
            {table:'incorporaciones', fields:['operativo','archivo'], abr:'I'},
        ],
        sql:{
            isTable:true,
            from:`(select *, '0' as borrar_v from archivos)`
            /*
            from:`(
                select * from archivos x,
                  lateral (select count(valor)::text as borrar_v from valores v where v.operativo = x.operativo and v.archivo=x.archivo) e
            )`,
            */
        }
        
    };
}