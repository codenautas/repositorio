"use strict";

import * as Path from "path";
import * as fs from "fs-extra";
import * as yauzl from "yauzl";

import "./types-repositorio";

import { serie } from "best-globals";

import { ProcedureDef } from "backend-plus";

import { Readable } from "stream";
import { createReadStream } from "fs-extra";
import * as likeAr from "like-ar";

// import {  LineSplitter, EscapeCharsTransform, LineJoiner /*, streamSignalsDone*/ } from "line-splitter";
import { EscapeCharsTransform, streamSignalsDone } from "line-splitter";

var WORK='work';
// var EXTS='exts';
var LIMIT_AUTO_DETECT = 100000;
// var SEPARATOR_CANDIDATES = [',',';','|','\t']; POR AHORA EN PG: [',',';','|',chr(9)]

var procedures: ProcedureDef[] = [
    {
        action:'documento_upload',
        multipart:true,
        progress:true,
        parameters:[
            {name:'primaryKeyValues'},
        ],
        files:{count:1},
        roles:['admin'],
        coreFunction:async function(context, parameters, files){
            var be=context.be;
            context.doing="opening file";
            var fileName:string;
            fileName=Path.basename(files![0].originalFilename);
            await context.client.query(`
                UPDATE archivos 
                  SET subido = null
                  WHERE operativo=$1 AND archivo=$2 
                  RETURNING true`, parameters.primaryKeyValues
            ).fetchUniqueRow();
            if(fileName!=parameters.primaryKeyValues[1]){
                throw new Error("No coincide el nombre del archivo");
            }
            var path=parameters.primaryKeyValues[0];
            var destName=`./${be.config.server.paths.attachments}/${path}/${fileName}`;
            await fs.move(files![0].path, destName, {overwrite:true});
            var result = await context.client.query(`
                UPDATE archivos 
                  SET subido = current_timestamp
                  WHERE operativo=$1 AND archivo=$2 
                  RETURNING archivo, subido`,parameters.primaryKeyValues).fetchUniqueRow();
            return result.row;
        }
    },
    {
        action:'incorporacion_incorporar',
        multipart:true,
        progress:true,
        parameters:[
            {name:'primaryKeyValues'},
        ],
        files:{count:1},
        roles:['admin'],
        coreFunction:async function(context, parameters, _files){
            var {be, client}=context;
            var {db}=be;
            context.informProgress({message:'iniciando la incorporación'});
            var {row:{
                operativo, archivo, renglon, metodo, origen, tabla_lineas, preserva_intermedios, destino, 
                separador, delimitador_texto, texto_de_null,
                punto_decimal, barra_fecha, formato_fecha
            }}=await client.query(`
                SELECT * 
                  FROM incorporaciones INNER JOIN archivos USING (operativo, archivo)
                       INNER JOIN operativos USING (operativo)
                  WHERE operativo=$1 AND archivo=$2 AND renglon=$3`, 
                  parameters.primaryKeyValues
            ).fetchUniqueRow();
            if(tabla_lineas){
                await client.query(`drop table if exists ${WORK}.${be.db.quoteIdent(tabla_lineas+'_lin')}`).execute();
                await client.query(`drop table if exists ${WORK}.${be.db.quoteIdent(tabla_lineas+'_col')}`).execute();
            }
            tabla_lineas=`import/${operativo}/${archivo+(origen?'/'+origen:'')}`.slice(0,57);
            var intentos=0;
            do{
                var {rowCount} = await client.query(
                    `select true from incorporaciones where tabla_lineas = $1`,
                    [tabla_lineas]
                ).fetchOneRowIfExists();
                intentos++;
                if(rowCount){
                    tabla_lineas = tabla_lineas.slice(0,57-intentos)+(Math.random().toString().slice(2,2+intentos));
                }
            }while(rowCount);
            var qTablaLineas=be.db.quoteIdent(tabla_lineas+'_lin');
            var qTablaColumnas=be.db.quoteIdent(tabla_lineas+'_col');
            await client.query(
                `update incorporaciones set tabla_lineas = $4 where (operativo, archivo, renglon) = ($1, $2, $3)`,
                [operativo, archivo, renglon, tabla_lineas]
            ).execute();
            var baseDir = Path.join(be.rootPath,be.config.server.paths.attachments,operativo);
            var workingDir = Path.join(be.rootPath,be.config.server.paths.working,operativo);
            var fileName=archivo;
            if(metodo=='tabla_txt'){
                let desdeZip=archivo.toLowerCase().endsWith('.zip');
                if(desdeZip){
                    context.informProgress({message:'descomprimiendo el archivo'});
                    var found:boolean=false;
                    var readStream=await new Promise<Readable>(function(resolve, reject){
                        yauzl.open(Path.join(baseDir,fileName), {lazyEntries: true}, function(err, zipfile){
                            if(err){
                                throw err;
                            }
                            if(!zipfile){
                                throw new Error("can not open zip file: procedures-repositorio")
                            }
                            zipfile.readEntry();
                            zipfile.on("entry", function(entry) {
                                if (/\/$/.test(entry.fileName)) {
                                    zipfile.readEntry();
                                } else if(entry.fileName==origen || entry.fileName.endsWith('/'+origen)){
                                    found=true;
                                    fileName=origen.replace(/\//g,'_');
                                    zipfile.openReadStream(entry, function(err, readStream) {
                                        if (err) throw err;
                                        readStream!.on("end", function() {
                                            // zipfile.readEntry();
                                        });
                                        resolve(readStream);
                                    });
                                }else{
                                    zipfile.readEntry();
                                }
                            });
                            zipfile.on("end", function() {
                                if(!found){
                                    reject(new Error("origen no encontrado dentro del archivo comprimido"));
                                }
                            });
                        });
                    })
                    context.informProgress({message:'registrando el archivo descomprimido'})
                    await fs.ensureDir(workingDir);
                    if(preserva_intermedios || true){
                        var ws=fs.createWriteStream(Path.join(workingDir,fileName));
                        // readStream.pipe(ws);
                        await new Promise(function(resolve){
                            ws.on('finish', function(){
                                console.log('resuelto');
                                resolve('ok!');
                            })
                            readStream.pipe(ws);
                        });
                        readStream = createReadStream(Path.join(workingDir,fileName))
                    }
                }else{
                    readStream = createReadStream(Path.join(baseDir,fileName))
                }
                var escapedStream=readStream
                    // .pipe(new LineSplitter({}))
                    .pipe(new EscapeCharsTransform({
                        charsToEscape:'\\', prefixChar:'\\'
                    }))
                    // .pipe(new LineJoiner({}));
                context.informProgress({message:'levantando líneas a la base de datos'});
                await client.query(`drop table if exists ${WORK}.${qTablaLineas}`).execute();
                await client.query(`create table ${WORK}.${qTablaLineas} (line_order bigserial primary key, line text)`).execute();
                await new Promise(function(resolve, _reject){
                    client.copyFromInlineDumpStream({
                        table:`${WORK}.${qTablaLineas}`, 
                        columns:['line'], 
                        inStream:escapedStream, 
                        done:resolve
                    })
                });
                var cantidad_columnas:number;
                if(!separador){
                    var result = await client.query(`
                        select * from (
                          select signo, min(length(line)-length(translate(line,signo,''))) as cantidad_minima, 
                                        min(case when line_order=1 then length(line)-length(translate(line,signo,'')) else null end) as cantidad_primera_linea
                            from unnest(array[',',';','|',chr(9)]) signo,
                                 lateral (select *
                                            from ${WORK}.${qTablaLineas}
                                            where line_order<$1) x
                            group by signo
                          ) x
                          where cantidad_primera_linea<=cantidad_minima
                          order by cantidad_primera_linea desc
                          limit 1
                 `, [LIMIT_AUTO_DETECT/*, SEPARATOR_CANDIDATES*/]).fetchOneRowIfExists();
                    if(!result.row){
                        throw new Error("No se puede detectar el separador (de columnas)");
                    }
                    separador = result.row.signo;
                    cantidad_columnas = result.row.cantidad_primera_linea+1;
                }else{
                    var result = await client.query(`
                        select min(length(line)-length(translate(line,$1,''))) as cantidad_primera_linea
                          from ${WORK}.${qTablaLineas}
                          where line_order=1
                    `, [separador]).fetchOneRowIfExists();
                    if(!result.row){
                        throw new Error("No hay lineas en la tabla importada");
                    }
                    cantidad_columnas = result.row.cantidad_primera_linea+1;
                }
                context.informProgress({message:'separando registros'});
                await client.query(`drop table if exists ${WORK}.${qTablaColumnas}`).execute();
                await client.query(`
                    create table ${WORK}.${qTablaColumnas} (
                        ${serie(1,cantidad_columnas).map((n:number)=>`col${n} text,
                        `).join('')}
                        line_order bigserial primary key,
                        header boolean 
                    )
                `).execute();
                let stream=fs.createReadStream(Path.join(desdeZip?workingDir:baseDir,fileName));
                await new Promise(function(resolve,reject){
                    var copying = client.copyFromInlineDumpStream({
                        table:`${WORK}.${qTablaColumnas}`, 
                        columns:serie(1,cantidad_columnas).map((n:number)=>`col${n}`), 
                        inStream:stream, 
                        with:` (FORMAT csv, DELIMITER ${db.quoteLiteral(separador)}${delimitador_texto?`, QUOTE ${db.quoteLiteral(delimitador_texto)}`:''}, NULL ${db.quoteLiteral(texto_de_null||'')} ) `
                    })
                    // @ts-ignore
                    streamSignalsDone(copying).then(resolve, reject)
                })
                await client.query(`update ${WORK}.${qTablaColumnas} set header=true where line_order=1`).execute();
                context.informProgress({message:'detectando tipos de columna antes'});
                await client.query(`
                    select ${serie(1,cantidad_columnas).map((n:number)=>`col${n}`).join(',')} 
                      from  ${WORK}.${qTablaColumnas} 
                      where header
                `).onRow(function(row){

                });
                context.informProgress({message:'detectando tipos de columna'});
                var {row:rowNombres} = await client.query(`
                    select ${serie(1,cantidad_columnas).map((n:number)=>`col${n}`).join(',')} 
                      from  ${WORK}.${qTablaColumnas} 
                      where header
                `).fetchUniqueRow();
                var {row:rowTipos}   = await client.query(`
                    select ${serie(1,cantidad_columnas).map((n:number)=>`to_jsonb(detect_type_agg(detect_type(col${n},${db.quoteLiteral(punto_decimal+barra_fecha+formato_fecha+'4')}))) as col${n}`).join(',')} 
                      from  ${WORK}.${qTablaColumnas} 
                      where header is not true
                `).fetchUniqueRow();
                let ordenVariable=0;
                let columnas=likeAr(rowNombres).map(function(nombreConAcentos:string, columna:string){
                    var tipoDetectado=rowTipos[columna];
                    var [tipoVar, tipoDb]  = tipoDetectado.bigint ? ['numero', 'bigint'] :(
                                tipoDetectado.decimal ? ['decimal','decimal'] :(
                                tipoDetectado.date ? ['fecha','date'] :(
                                ['texto', 'text']
                    )));
                    ordenVariable++;
                    var nombre=nombreConAcentos.toLowerCase()
                        .replace(/[áÁàÀäÄâÂãÃ]/g,'a')
                        .replace(/[çÇ]/g,'c')
                        .replace(/[éÉèÈëËêÊ]/g,'e')
                        .replace(/[íÍìÌïÏîÎ]/g,'i')
                        .replace(/[óÓòÒöÖôÔõÕ]/g,'o')
                        .replace(/[úÚùÙüÜûÛ]/g,'u')
                        .replace(/[ñÑ]/g,'nni')
                        .replace(/[ýÝÿ]/g,'y')
                        .replace(/[^a-z0-9_]/g,'_');
                    return {nombre, tipoVar, tipoDb, ordenVariable, colN:columna}
                }).array();
                context.informProgress({message:'creando el diseño de registro'});
                await be.procedure.table_record_save.coreFunction(
                    context,{
                        table: 'tabla_datos', 
                        primaryKeyValues: [],
                        newRow:{operativo, tabla_datos:destino, tipo:'externa'},
                        oldRow:[],
                        status:'update',
                        insertIfNotUpdate:true,
                        masive:true
                    }
                );
                await client.query(`delete from variables where operativo = $1 and tabla_datos = $2`,[operativo,destino]).execute();
                await client.bulkInsert({
                    table:'variables',
                    columns:['operativo', 'tabla_datos', 'variable', 'tipovar', 'clase', 'activa', 'orden', 'es_pk' ],
                    rows:[[
                        operativo, destino, 'NUM', 'numero', 'externa', true, 0, 1
                    ]].concat(columnas.map(({nombre, tipoVar, ordenVariable})=>
                        [operativo, destino, nombre, tipoVar, 'externa', true, ordenVariable, null]
                    ))
                })
                context.informProgress({message:'generando la tabla de datos'});
                await be.procedure.tabla_datos_generar.coreFunction(context,{
                    operativo, 
                    tabla_datos:destino
                });
                await client.query(`create sequence ${db.quoteIdent(`${operativo}_${destino}_seq`.toLowerCase())}`).execute();
                await client.query(`alter table ${db.quoteIdent(`${operativo}_${destino}`.toLowerCase())} 
                    alter column "NUM" set default nextval(${db.quoteLiteral(`${operativo}_${destino}_seq`.toLowerCase())}::regclass)
                `).execute();
                if(formato_fecha=='f'){
                    await client.query(`set datestyle to "dmy, iso"`).execute();
                }else if(formato_fecha=='e'){
                    await client.query(`set datestyle to "mdy, iso"`).execute();
                }else{
                    await client.query(`set datestyle to "iso"`).execute();
                }
                stream=fs.createReadStream(Path.join(desdeZip?workingDir:baseDir,fileName));
                await new Promise(function(resolve,reject){
                    var copying = client.copyFromInlineDumpStream({
                        table:`${db.quoteIdent(`${operativo}_${destino}`.toLowerCase())}`, 
                        columns:columnas.map(({nombre})=>nombre), 
                        inStream:stream, 
                        with:` (HEADER, FORMAT csv, DELIMITER ${db.quoteLiteral(separador)}${delimitador_texto?`, QUOTE ${db.quoteLiteral(delimitador_texto)}`:''}, NULL ${db.quoteLiteral(texto_de_null||'')} ) `
                    })
                    // @ts-ignore
                    streamSignalsDone(copying).then(resolve, reject)
                })
                // await client.query(`
                //     INSERT INTO ${db.quoteIdent(`${operativo}_${destino}`.toLowerCase())} (${columnas.map(x=>x.nombre).join(',')},"NUM")
                //         SELECT ${columnas.map(x=>`${x.colN}::${x.tipoDb}`).join(',')},line_order
                //         FROM ${WORK}.${qTablaColumnas}
                //         WHERE header IS NOT TRUE;
                // `).execute();
                    /*
8 1:00 2:35 generando la tabla de datos
7 0:00 1:35 creando el diseño de registro
6 1:32 1:34 detectando tipos de columna
5 0:00 0:01 separando registros
4 0:01 0:01 levantando líneas a la base de datos
3 0:00 0:00 registrando el archivo descomprimido
2 0:00 0:00 descomprimiendo el archivo
1 0:00 0:00 iniciando la incorporación

8 0:59 2:31 generando la tabla de datos
7 0:00 1:31 creando el diseño de registro
6 1:29 1:31 detectando tipos de columna
5 0:00 0:01 separando registros
4 0:00 0:01 levantando líneas a la base de datos
3 0:00 0:00 registrando el archivo descomprimido
2 0:00 0:00 descomprimiendo el archivo
1 0:00 0:00 iniciando la incorporación

                    8 1:02 2:41 generando la tabla de datos
                    7 0:00 1:38 creando el diseño de registro
                    6 1:36 1:38 detectando tipos de columna
                    5 0:00 0:01 separando registros
                    4 0:00 0:01 levantando líneas a la base de datos
                    3 0:00 0:00 registrando el archivo descomprimido
                    2 0:00 0:00 descomprimiendo el archivo
                    1 0:00 0:00 iniciando la incorporación
                    */
                /*
                await client.query(`
                  do $body$
                  begin
                    INSERT INTO ${db.quoteIdent(`${operativo}_${destino}`.toLowerCase())} (${columnas.map(x=>x.nombre).join(',')},"NUM")
                        SELECT ${columnas.map(x=>
                                x.tipo!='fecha' || formato_fecha!='i'?`${x.colN}::${x.tipo}`:
                                (`to_date(${x.colN},${formato_fecha!='e'?`'dd${barra_fecha}mm${barra_fecha}yyyy'`:`'mm${barra_fecha}dd${barra_fecha}yyyy'`})`)
                            ).join(',')},line_order
                        FROM ${WORK}.${qTablaColumnas}
                        WHERE header IS NOT TRUE;
                    create table work.result as select 'ok';
                  exception
                    when others then
                      create table work.result as select ;
                  end;
                  $body$
                `).execute();
                await client.query(`CREATE SEQUENCE ${db.quoteIdent(`${operativo}_${destino}_seq`)} ALTER COLUMN `).execute();
                await client.query(`ALTER TABLE ${db.quoteIdent(`${operativo}_${destino}`)} ALTER COLUMN __i__ SET DEFAULT nextval(${db.quoteLiteral(`${operativo}_${destino}_seq`)}::regclass) `).execute();
                context.informProgress({message:'llenando la tabla de datos'});
                stream=fs.createReadStream(Path.join(workingDir,fileName));
                await new Promise(function(resolve,reject){
                    var copying = client.copyFromInlineDumpStream({
                        table:`${operativo}_${destino}`, 
                        columns:likeAr(rowNombres).array(), 
                        inStream:stream, 
                        with:` (FORMAT csv, DELIMITER ${db.quoteLiteral(separador)}${delimitador_texto?`, QUOTE ${db.quoteLiteral(delimitador_texto)}`:''}, NULL ${db.quoteLiteral(texto_de_null||'')} ) `
                    })
                    // @ts-ignore
                    streamSignalsDone(copying).then(resolve, reject)
                })
                */
            }else{
                throw new Error('no existe el metodo de incorporacion');
            }
            return "listo";
        }
    },
];

export {procedures};