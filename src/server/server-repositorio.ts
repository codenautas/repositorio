"use strict";

import { AppBackend } from "backend-plus";
import { emergeAppRepositorio } from "./app-repositorio";
import { emergeAppDatosExt } from "datos-ext";
import { emergeAppOperativos } from "operativos";

var AppRepositorio = emergeAppRepositorio(emergeAppDatosExt(emergeAppOperativos(AppBackend)));

new AppRepositorio().start();