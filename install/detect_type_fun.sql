﻿-- work."import/EPH2018T2/EPH_usu_2_Trim_2018_txt.zip/usu_hogar_t8_col"

-- /*
create type detect_type_type as (
  "not_null" boolean,
  "integer"  boolean,
  "bigint"   boolean,
  "decimal"  boolean,
  "date"     boolean
);
-- */

create or replace function detect_type(p_texto text, p_options text) returns detect_type_type 
  language sql 
as $sql$
  select p_texto is not null as not_null,
         p_texto ~ '^-?[12]?\d{0,4}$' as "integer",
         p_texto ~ '^-?\d{0,9}$' as "bigint",
         p_texto ~ ('^-?\d+\'||substr(p_options,1,1)||'?\d*$') or 
         p_texto ~ ('^-?\d*\'||substr(p_options,1,1)||'?\d+$') as "decimal",
         substr(p_options,3,2)='f4' and p_texto ~ ('^[0123]?\d'||substr(p_options,2,1)||'[01]?\d'||substr(p_options,2,1)||'\d{4}$') or
         substr(p_options,3,2)='e4' and p_texto ~ ('^[01]?\d'||substr(p_options,2,1)||'[0123]?\d'||substr(p_options,2,1)||'\d{4}$') or
         substr(p_options,3,2)='i4' and p_texto ~ ('^\d{4}'||substr(p_options,2,1)||'[01]?\d'||substr(p_options,2,1)||'[0123]?\d$') as "date";
$sql$;

-- select to_jsonb('(f,f,f,f,f)'::detect_type_type)

select *
  from ( 
select to_jsonb('(t,f,f,f,f)'::detect_type_type)||expected::jsonb, case when jsonb_populate_record(null::detect_type_type,to_jsonb('(t,f,f,f,f)'::detect_type_type)||expected::jsonb) is distinct from  detect_type(p_texto, p_options) then 'ERROR' else '' end as resultado, expected, p_texto, p_options, to_jsonb(detect_type(p_texto, p_options))
  from (
select 'hola' as p_texto, '.-e4' as p_options, '{}' as expected 
  union select    '123'       , '.-e4', '{"integer":true, "bigint":true, "decimal":true}'
  union select    '-123'      , '.-e4', '{"integer":true, "bigint":true, "decimal":true}'   
  union select    '-123333'   , '.-e4', '{                "bigint":true, "decimal":true}'   
  union select    '123.44'    , '.-e4', '{                               "decimal":true}'    
  union select    '123,44'    , ',-e4', '{                               "decimal":true}'
  union select    '123.44'    , ',-e4', '{}'
  union select    null        , './f4', '{"not_null": false, "integer":null, "bigint":null, "decimal":null, "date":null}'
  union select    '12/11/2010', './f4', '{"date": true}'
  union select    '22/11/2010', './e4', '{}' 
  union select    '2010-12-11', '.-i4', '{"date": true}'
  union select    '2010-12-11', '.-f4', '{}' ) casos
  ) pruebas
  where resultado is distinct from '';

create or replace function detect_type_and(a detect_type_type, b detect_type_type) returns detect_type_type
  language sql 
as $sql$
  select coalesce(a.not_null and b.not_null, a.not_null, b.not_null) as not_null,
         coalesce(a.integer  and b.integer , a.integer , b.integer ) as integer ,
         coalesce(a.bigint   and b.bigint  , a.bigint  , b.bigint  ) as bigint  ,
         coalesce(a.decimal  and b.decimal , a.decimal , b.decimal ) as decimal ,
         coalesce(a.date     and b.date    , a.date    , b.date    ) as date;
$sql$;

create aggregate detect_type_agg(detect_type_type)(
  stype = detect_type_type,
  sfunc = detect_type_and
);

select detect_type_agg(detect_type(a,'./i4')), detect_type_agg(detect_type(b,'./i4'))
  from (
    select '12' as a, '12.3' as b
    union select '32', '44'
    union select '-2', null
  ) casos;
  
/*
select detect_type_agg(detect_type(col1,'./i4'))
   ,detect_type_agg(detect_type(col2,'./i4'))
   ,detect_type_agg(detect_type(col3,'./i4'))
   ,detect_type_agg(detect_type(col4,'./i4'))
   ,detect_type_agg(detect_type(col5,'./i4'))
  from work."import/EPH2018T2/EPH_usu_2_Trim_2018_txt.zip/usu_hogar_t8_col"
  where header is not true
  limit 100;
*/