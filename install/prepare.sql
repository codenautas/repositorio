create schema if not exists work;
grant usage on schema work to repositorio_user;
grant create on schema work to repositorio_user;

create schema if not exists exts;
grant usage on schema exts to repositorio_user;
grant create on schema exts to repositorio_user;
